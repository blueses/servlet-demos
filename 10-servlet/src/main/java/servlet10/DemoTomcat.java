package servlet10;

import cn.hutool.core.util.ReflectUtil;
import lombok.NoArgsConstructor;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor
public class DemoTomcat {

    private int port = 8080;

    private Map<String, String> urlServletMap = new HashMap<>();

    public DemoTomcat(int port) {
        this.port = port;
    }


    int i = 0;


    public void start() {

        initServletMapping();
        ServerSocket server = null;


        try {
            server = new ServerSocket(port);
            System.out.println("my tomcat is start......");

            while (true) {
                Socket socket = server.accept();
                handleRequest(socket, i++);

//                InputStream inputStream = socket.getInputStream();
//                System.out.println("333333333333333333");
//                OutputStream outputStream = socket.getOutputStream();
//                System.out.println("4444444444444444444444");
//
//
//                HttpServletRequest
//
//                HttpServletRequest request = new HttpServletRequest(inputStream);
//
//                RequestDispatcher
//
//                String httpRequest = HttpUtil.getString(inputStream, Charset.defaultCharset(), true);
//
//                System.out.println("5555555555555555555555555");
//                System.out.println(httpRequest);
//
//                System.out.println("666666666666666666666666666666");
//
////                //按回车拆分字符串为数组
////                String[] requestArray = httpRequest.split("\r\n");
////                String httpHead = requestArray[0];
////                //按空格拆分字符串为数组
////                String[] headArray = httpHead.split("\\s+");
////                String uri = headArray[1];
////                String method = headArray[0];
////
//                HttpServletRequest request = new HttpServletRequestWrapper(null) {
//                    @Override
//                    public String getRequestURI() {
//                        return "/hello";
//                    }
//
//                    @Override
//                    public String getMethod() {
//                        return "GET";
//                    }
//
//                    @Override
//                    public ServletRequest getRequest() {
//                        return super.getRequest();
//                    }
//                };
////
////
//                HttpServletResponse response = new HttpServletResponseWrapper(null) {
//                    @Override
//                    public String getContentType() {
//                        return "text/html";
//                    }
//                };
//
//
////                MyRequest request = new MyRequest(inputStream);
////                MyResponse response = new MyResponse(outputStream);
////
//                dispatch(request, response);
//
//                socket.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void handleRequest(Socket clientSocket, int clientNo) {
        PrintStream os;
        BufferedReader in;

        try {
            in =new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            os=new PrintStream(clientSocket.getOutputStream());

        } catch (Exception e) {
            System.out.println("连接[" + clientNo + "]关闭");
        }

    }


    private void initServletMapping() {
        for (ServletMapping servletMapping : ServletMappingConfig.servletMappingList) {
            urlServletMap.put(servletMapping.getUrl(), servletMapping.getClazz());
        }
    }


    private void dispatch(HttpServletRequest request, HttpServletResponse response) {
        String clazzName = urlServletMap.get(request.getRequestURI());
        try {
            HttpServlet servlet = ReflectUtil.newInstance(clazzName);
            servlet.service(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        new DemoTomcat(8080).start();
    }
}
