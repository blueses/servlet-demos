package servlet07;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;

@WebServlet("/hello7/*")
public class Hello extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");

        String name = request.getParameter("name");

        PrintWriter out = response.getWriter();
        out.print("<!DOCTYPE html>");
        out.print("<html>");
        out.print("<head>");
        out.print("<title>Hello</title>");
        out.print("</head>");
        out.print("<body>");
        out.printf("<h1>访问时间：%s</h1>", LocalDateTime.now().toString());
        out.printf("<h1>访问IP(remote addr):%s</h1>", request.getRemoteAddr());
        out.printf("<h1>访问端口(remote port):%s</h1>", request.getRemotePort());

        out.printf("<h1>访问IP(local addr):%s</h1>", request.getLocalAddr());
        out.printf("<h1>访问端口(local port):%s</h1>", request.getLocalPort());

        out.printf("<h1>访问端口(server port):%s</h1>", request.getServerPort());


        out.printf("<h1>访问地址:%s</h1>", request.getRequestURI());

        out.printf("<h1>访问参数:%s</h1>", name);
        out.print("</body>");
        out.print("</html>");
    }
}
