package servlet03;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 01 和 02 两个示例中使用的是 @WebServlet 注解
 * <p>
 * 此例子演示的是使用早期版本的 web.xml 进行配置
 * <p>
 * 其中 <servlet>标签定义的是servlet名字，类路径和loadOnStartup属性
 * <p>
 * <servlet-mapping>标签定义的是访问路径,和注解一样，路径前面的斜杠也不能去掉
 */
public class Hello extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String name = request.getParameter("name");

        PrintWriter out = response.getWriter();
        out.print("<!DOCTYPE html>");
        out.print("<html>");
        out.print("<head>");
        out.print("<title>Hello</title>");
        out.print("</head>");
        out.print("<body>");
        out.printf("<h1> Hello!====03[%s]!%n</h1>", name);
        out.print("</body>");
        out.print("</html>");
    }
}
