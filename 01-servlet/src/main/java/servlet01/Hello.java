package servlet01;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


/**
 * <p>
 * 请求这个servlet这个资源的时候 url : http://localhost:8080/hello?name=Jack
 * <p>
 * 打成war包放到tomcat里面就可以直接访问 /hello 接口，还可以在后面拼上name参数
 */
@WebServlet("/hello")
public class Hello extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String name = request.getParameter("name");

        PrintWriter out = response.getWriter();
        out.print("<!DOCTYPE html>");
        out.print("<html>");
        out.print("<head>");
        out.print("<title>Hello</title>");
        out.print("</head>");
        out.print("<body>");
        out.printf("<h1> Hello!===[%s]!%n</h1>", name);
        out.print("</body>");
        out.print("</html>");

        //out.append("我的第一个web程序");
        //System.out.println("hello,world");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //我不管你的请求方式是get还是post请求 都到这个里面
        doGet(request, response);
    }
}
