package servlet05;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 这种注解的路径配置方式，表示只要路径以/servlet 开头的都可以访问到这里
 * <p>
 * 例如  /servlet/01   /servlet/02  等等
 */
@WebServlet("/servlet/*")
public class Hello extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        out.print("<!DOCTYPE html>");
        out.print("<html>");
        out.print("<head>");
        out.print("<title>Hello</title>");
        out.print("</head>");
        out.print("<body>");
        out.printf("<h1>%s</h1>", request.getRequestURI());
        out.printf("<h1>%s</h1>", request.getContextPath());
        out.printf("<h1>%s</h1>", request.getServletPath());
        out.printf("<h1>%s</h1>", request.getPathInfo());
        out.print("</body>");
        out.print("</html>");
    }
}
