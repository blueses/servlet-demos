package servlet09;

import lombok.Getter;

import java.io.IOException;
import java.io.InputStream;

public class MyRequest {

    @Getter
    private String uri;

    @Getter
    private String method;

    public MyRequest(InputStream inputStream) throws IOException {

        String httpRequest = "";
        byte[] httpRequestBytes = new byte[1024];
        int length = 0;
        if ((length = inputStream.read(httpRequestBytes)) > 0) {
            httpRequest = new String(httpRequestBytes, 0, length);
        }


        //按回车拆分字符串为数组
        String[] requestArray = httpRequest.split("\r\n");
        String httpHead = requestArray[0];
        //按空格拆分字符串为数组
        String[] headArray = httpHead.split("\\s+");
        uri = headArray[1];
        method = headArray[0];

        System.out.println(this);

    }

    @Override
    public String toString() {
        return "MyRequest{" + "uri='" + uri + '\'' + ", method='" + method + '\'' + '}';
    }
}
