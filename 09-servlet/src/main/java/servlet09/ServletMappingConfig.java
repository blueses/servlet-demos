package servlet09;

import java.util.ArrayList;
import java.util.List;

public class ServletMappingConfig {

    public static List<ServletMapping> servletMappingList = new ArrayList<>();

    static {
        servletMappingList.add(new ServletMapping("findGirl", "/girl", "servlet09.FindGirlServlet"));
        servletMappingList.add(new ServletMapping("helloWorld", "/hello", "servlet09.HelloWorldServlet"));
    }

}
