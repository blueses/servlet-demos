package servlet02;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @WebServlet(name = "Hello")
 * <p>
 * 告诉服务器这个是我的servlet
 * @webServlet 是我们请求servlet的入口
 * urlPatterns 表示访问路径 前面需要加一个斜线,如果这个斜线没有带会报错
 * @WebServlet("/hello") 这样只表示路径
 * <p>
 * <p>
 * 定义加载顺序：loadOnStartup 的值是一个整数，表示 Servlet 初始化时的优先级或顺序。较小的数值表示优先级较高，即应该先加载和初始化。如果多个 Servlet 都有设置 loadOnStartup，则按照数字大小的顺序加载。
 * <p>
 * 延迟加载控制：如果 loadOnStartup 的值为负数或者没有设置，默认情况下 Servlet 是在第一次请求该 Servlet 时才会被实例化（延迟加载）。如果设置了正数的 loadOnStartup 值，容器会在启动时立即加载并初始化这个 Servlet，而不是等到第一次请求。
 * <p>
 * 初始化顺序控制： 在一些情况下，特定 Servlet 的初始化可能依赖于其他 Servlet 或者资源的初始化。通过设置合适的 loadOnStartup 值，可以控制 Servlet 的初始化顺序，确保依赖关系正确处理。
 */
@WebServlet(name = "Hello", urlPatterns = "/hello", loadOnStartup = 1)
public class Hello extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String name = request.getParameter("name");

        PrintWriter out = response.getWriter();
        out.print("<!DOCTYPE html>");
        out.print("<html>");
        out.print("<head>");
        out.print("<title>Hello</title>");
        out.print("</head>");
        out.print("<body>");
        out.printf("<h1> Hello!===[%s]!%n</h1>", name);
        out.print("</body>");
        out.print("</html>");
    }
}
