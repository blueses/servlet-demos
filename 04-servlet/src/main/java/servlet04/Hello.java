package servlet04;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 这里使用注解并且web.xml文件中也同时配置了name=Hello的servlet，那么这种情况会优先使用web.xml中的路径，这里的路径不生效
 */
@WebServlet(name = "Hello", urlPatterns = "/hello", loadOnStartup = 1)
public class Hello extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String name = request.getParameter("name");

        PrintWriter out = response.getWriter();
        out.print("<!DOCTYPE html>");
        out.print("<html>");
        out.print("<head>");
        out.print("<title>Hello</title>");
        out.print("</head>");
        out.print("<body>");
        out.printf("<h1> Hello04!===[%s]!%n</h1>", name);
        out.print("</body>");
        out.print("</html>");
    }
}
